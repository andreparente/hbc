//
//  CheckListTableViewCell.swift
//  HBC
//
//  Created by Andre Machado Parente on 09/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class CheckListTableViewCell: UITableViewCell {

    @IBOutlet weak var observacaoTxtView: UITextView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var okNotOkSwitch: UISwitch!
    @IBOutlet weak var okNotOKLbl: UILabel!
    
    var switchTapAction: ((Bool)->Void)?
    var observacaoTxtViewEndEditingAction: ((String)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.observacaoTxtView.text = "Observação"
        self.observacaoTxtView.textColor = UIColor.lightGray
        self.observacaoTxtView.delegate = self
        self.observacaoTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.observacaoTxtView.layer.borderWidth = 1
        self.observacaoTxtView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func switched(_ sender: UISwitch) {
        print("Switched: \(sender.isOn)")
        
        if sender.isOn {
            self.okNotOKLbl.text = "Ok"
        } else {
            self.okNotOKLbl.text = "Not Ok"
        }
        // send the Switch state in a "call back" to the view controller
        switchTapAction?(sender.isOn)
    }
}

extension CheckListTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text == "" {
            textView.text = "Observação"
            textView.textColor = UIColor.lightGray
        } else {
            observacaoTxtViewEndEditingAction?(textView.text)
        }
    }
}
