//
//  DefaultObservationTableViewCell.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class DefaultObservationTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var descricao: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.descricao.layer.borderColor = UIColor.lightGray.cgColor
        self.descricao.layer.borderWidth = 1
        self.descricao.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
