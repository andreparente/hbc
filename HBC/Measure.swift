//
//  Measure.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation


class Measure {
    var title: String!
    var equipment: Equipment!
    var observations: [Observation] = []
    var generalObservation: String!
    var unit: String!
    var obsAdded: Bool = false
    var id: String!
    
    init(title: String) {
        self.title = title
        switch title {
        case "Medição de C.M e C.E":
            self.unit = "V/m ou nt"
        case "Medição de COV":
            self.unit = "HCOH ou TVOC"
        case "Umidade Relativa do Ar":
            self.unit = "Rh"
        case "Sonoridade":
            self.unit = "dB"
        case "Iluminação (Lux)":
            self.unit = "Lux"
        case "Microondas":
            self.unit = "MHZ"
        case "Teste de Fungo":
            self.unit = ""
        case "PH da água":
            self.unit = "Ph"
        case "Bússola":
            self.unit = "Variação"
        default:
            self.unit = ""
        }
    }
}
