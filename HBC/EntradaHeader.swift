//
//  EntradaHeader.swift
//  HBC
//
//  Created by Andre Machado Parente on 18/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

protocol EntradaHeaderDelegate: class {
    func addressTxtFieldChanged(text: String)
    func clientNameTxtFieldChanged(text: String)

}

class EntradaHeader: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionTxtView: UITextView!
    @IBOutlet weak var clientNameTxtField: UITextField!
    @IBOutlet weak var clientAddressTxtField: UITextField!

    weak var delegate: EntradaHeaderDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit() {
        Bundle.main.loadNibNamed("EntradaHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.clientAddressTxtField.delegate = self
        self.clientAddressTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        self.clientNameTxtField.delegate = self
        self.clientNameTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        self.descriptionTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.descriptionTxtView.layer.borderWidth = 1
        self.descriptionTxtView.layer.cornerRadius = 5

    }
}

extension EntradaHeader: UITextFieldDelegate {
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.isEqual(self.clientAddressTxtField) {
            delegate?.addressTxtFieldChanged(text: textField.text!)
        } else {
            delegate?.clientNameTxtFieldChanged(text: textField.text!)
        }
    }
}
