//
//  User.swift
//  HBC
//
//  Created by Andre Machado Parente on 16/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation
import SwiftyDropbox

class User {
    
    var name: String!
    var email: String!
    var visits: [Visit]!
    var id: String!
    
    var currentVisit: Visit?
    var dropBoxClient: DropboxClient?
    var dropboxLogged: Bool = false
    
    //singleton
    static let sharedInstance = User()
    
    private init() {
        self.visits = []
    }
    
    func authorizeDropbox() {
        self.dropBoxClient = DropboxClientsManager.authorizedClient
    }
}

