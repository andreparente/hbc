//
//  Extensions.swift
//  HBC
//
//  Created by Andre Machado Parente on 11/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    var customPink: UIColor {
        
        return UIColor(red:0.98, green:0.88, blue:0.93, alpha:1.0)
    }
    
    var placeholderColor: UIColor {
        return UIColor(red: 199/255, green: 199/255, blue: 205/255, alpha: 1)
    }
    
    var customLightBlue: UIColor {
        return UIColor(red:0.83, green:0.93, blue:0.99, alpha:1.0)
    }
    var customLightGreen: UIColor {
        return UIColor(red:0.77, green:0.89, blue:0.80, alpha:0.8)
    }
    var customLightYellow: UIColor {
        return UIColor(red: 248/255, green: 237/255, blue: 174/255, alpha: 1)
    }
    var customDarkGreen: UIColor {
        print("teste")
        return UIColor(red:0.60, green:0.72, blue:0.56, alpha:1.0)
    }
}

extension UIImageView: URLSessionDelegate {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        let loading = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        loading.center = self.center
        loading.color = UIColor().customLightGreen
        loading.hidesWhenStopped = true
        loading.startAnimating()
        self.addSubview(loading)
        
        session.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                
                else { return }
            DispatchQueue.main.async() { () -> Void in
                loading.stopAnimating()
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            let credential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
            challenge.sender?.use(credential, for: challenge)
            completionHandler(URLSession.AuthChallengeDisposition.useCredential, credential)
        }
    }
}


