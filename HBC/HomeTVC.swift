//
//  ViewController.swift
//  HBC
//
//  Created by Andre Machado Parente on 08/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit
import PDFGenerator
import SafariServices
import SwiftyDropbox

class HomeTVC: UITableViewController {

    var headerTable: HomeHeader!
    var indexSelected: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "Logo"))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "MenuSandwich"), style: .plain, target: self, action: #selector(HomeTVC.showSideMenu))
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor().customDarkGreen
        self.setHeader()
        
        self.tableView.register((UINib(nibName: "HomeTableViewCell", bundle: nil)), forCellReuseIdentifier: "HomeCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DAO.sharedInstance.fetchReports { (success: Bool, response: String) in
            if success {
                self.tableView.reloadData()
                self.headerTable.numVisitsLbl.text = "Qtd de visitas: " + String(User.sharedInstance.visits.count)
            } else {
                print("deu ruim")
            }
        }
    }
    
    func setHeader() {
        self.headerTable = HomeHeader(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
        self.headerTable.delegate = self
        self.tableView.tableHeaderView = headerTable
    }
    
    func showSideMenu() {
        
    }
    
        
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return User.sharedInstance.visits.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeTableViewCell
        cell.clientNameLbl.text = User.sharedInstance.visits[indexPath.row].clientName
        cell.localLbl.text = User.sharedInstance.visits[indexPath.row].clientAddress
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        label.text = "Visitas realizadas"
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: label.font.pointSize, weight: UIFontWeightSemibold)
        label.backgroundColor = UIColor().customDarkGreen
        return label
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexSelected = indexPath.row
        self.performSegue(withIdentifier: "HomeToReport", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToReport" {
            let destination = segue.destination as! ReportTVC
            print(indexSelected)
            User.sharedInstance.currentVisit = User.sharedInstance.visits[indexSelected]
            destination.report = User.sharedInstance.visits[indexSelected]
        }
    }
}

extension HomeTVC: HomeHeaderDelegate {
    
    func didTapNewReportButton() {
        User.sharedInstance.currentVisit = Visit()
        self.performSegue(withIdentifier: "HomeToFirst", sender: self)
    }
}
