//
//  DefaultObservationViewController.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class DefaultObservationViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var imageSelectedView: UIImageView!
    @IBOutlet weak var photoView: UIView!
    @IBOutlet weak var addPictureButton: UIButton!
    @IBOutlet weak var addPictureLbl: UILabel!
    @IBOutlet weak var takeOtherPictureButton: UIButton!
    @IBOutlet weak var observationTxtView: UITextView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    var room: Room!
    var auxObs: Observation!
    var cameFromFeedback: Bool = false
    var imagePicker: UIImagePickerController!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.takeOtherPictureButton.isHidden = true
        self.imageSelectedView.isHidden = true
        self.imageSelectedView.contentMode = .scaleAspectFill
        self.imageSelectedView.layer.masksToBounds = true
        self.auxObs = Observation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.observationTxtView.delegate = self
        self.observationTxtView.text = "Escreva aqui..."
        self.observationTxtView.textColor = UIColor.lightGray
        self.observationTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.observationTxtView.layer.borderWidth = 1
        self.observationTxtView.layer.cornerRadius = 5
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Take image
    @IBAction func addPicture(_ sender: UIButton) {
        if (imagePicker) != nil {
            
        } else {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
        }
        
        
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Escolha", preferredStyle: .actionSheet)
        
        // 2
        let newPictureAction = UIAlertAction(title: "Tirar nova Foto", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.imagePicker.sourceType = .camera
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)

        })
        
        let choosePictureAction = UIAlertAction(title: "Choose Image from album", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(newPictureAction)
        optionMenu.addAction(choosePictureAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }

    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let picture = info[UIImagePickerControllerOriginalImage] as? UIImage {
            auxObs.picture = picture
            self.takeOtherPictureButton.isHidden = false
            self.addPictureLbl.isHidden = true
            self.addPictureButton.isHidden = true
            self.imageSelectedView.image = auxObs.picture
            self.imageSelectedView.isHidden = false
            
        }
    }
    
    @IBAction func saveObservation(_ sender: Any) {
        //salvar e depois sair da tela
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let hour = calendar.component(.hour, from: date)
        let minute = calendar.component(.minute, from: date)
        let second = calendar.component(.second, from: date)
        let id = String(second) + String(minute) + String(hour) + String(day) + String(month) + String(year)
        auxObs.id = String(id)
        
        if cameFromFeedback {
            room.feedbacks.append(auxObs)
            room.obsAdded = true
            self.cameFromFeedback = false
            self.dismiss(animated: true, completion: nil)
            return
        } else {
            room.observations.append(auxObs)
            room.obsAdded = true
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension DefaultObservationViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text == "" {
            textView.text = "Escreva aqui..."
            textView.textColor = UIColor.lightGray
        } else {
            self.auxObs.descricao = textView.text!
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.auxObs.descricao = textView.text!
    }
}
