//
//  ParecerTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 06/11/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class FeedbackTVC: UITableViewController {

    var room: Room!
    var tableHeader: HeaderDefault!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.register(UINib(nibName: "SectionHeaderDefault", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderDefault")
        tableView.register(UINib(nibName: "FeedbackTableViewCell", bundle: nil), forCellReuseIdentifier: "FeedbackCell")

        self.setHeaderView()

    }
    
    func setHeaderView() {
        tableHeader = HeaderDefault(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
        tableHeader.title.isHidden = true
        tableHeader.text.text = "Preencha abaixo o feedback da solução final, para aparecer no relatório para o cliente, você pode adicionar ou não uma foto para cada item do parecer, fique à vontade."
        self.tableView.tableHeaderView = tableHeader
        
    }

    @IBAction func saveFeedbacks(_ sender: Any) {
        DAO.sharedInstance.saveFeedbackFor(room: room, report: User.sharedInstance.currentVisit!) { (success: Bool, response: String) in
            if success {
                // 1
                let optionMenu = UIAlertController(title: "Salvo com Sucesso!", message: nil, preferredStyle: .alert)
                
                
                //
                let okAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                    self.navigationController?.popToRootViewController(animated: true)
                })
                
                
                // 4
                optionMenu.addAction(okAction)
                
                // 5
                self.present(optionMenu, animated: true, completion: nil)

            } else {
                
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return room.feedbacks.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FeedbackCell", for: indexPath) as! FeedbackTableViewCell

        cell.feedbackTxtViewEndEditingAction = {
            (observacao) in
            if observacao == "Feedback" || observacao == "" {
                self.room.feedbacks[indexPath.row].descricao = ""
            } else {
                self.room.feedbacks[indexPath.row].descricao = observacao
            }
        }
        
        if room.feedbacks[indexPath.row].descricao == "" {
            cell.feedbackText.text = "Observação"
            cell.feedbackText.textColor = .lightGray
        } else {
            cell.feedbackText.text = room.feedbacks[indexPath.row].descricao
            cell.feedbackText.textColor = .black
        }
        
        if let picture = room.feedbacks[indexPath.row].picture {
            cell.picture.image = picture
            cell.picture.contentMode = .scaleAspectFill
            cell.picture.layer.masksToBounds = true
        } else {
            if cell.picture.image == nil {
                cell.picture.downloadedFrom(link: room.feedbacks[indexPath.row].urlPicture)
                cell.picture.contentMode = .scaleAspectFill
                cell.picture.layer.masksToBounds = true
            }
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionHeaderDefault") as! SectionHeaderDefault
        
        if room.feedbacks.count > 0 {
            headerView.title.text = "Feedbacks (\(room.feedbacks.count))"
        } else {
            headerView.title.text = "Feedbacks"
        }
        headerView.sectionNumber = section
        headerView.delegate = self
        headerView.contentView.backgroundColor = UIColor().customLightGreen
        return headerView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if room.obsAdded {
            self.tableView.reloadData()
            room.obsAdded = false
            return
        } else {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FeedbackTVC: SectionHeaderDefaultDelegate {
    
    func didTapAddButton(in section: Int) {
        print("\(section)")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultObservation") as! DefaultObservationViewController
        vc.room = room
        vc.cameFromFeedback = true
        self.present(vc, animated: true, completion: nil)
    }
}
