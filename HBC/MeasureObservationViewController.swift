//
//  MeasureObservationViewController.swift
//  HBC
//
//  Created by Andre Machado Parente on 13/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class MeasureObservationViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var addPictureLbl: UILabel!
    @IBOutlet weak var addPictureButton: UIButton!
    @IBOutlet weak var takeOtherPictureButton: UIButton!
    @IBOutlet weak var observationTxtView: UITextView!
    @IBOutlet weak var valueTxtField: UITextField!
    @IBOutlet weak var localTxtField: UITextField!
    @IBOutlet weak var imageSelectedView: UIImageView!
    
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    var imagePicker: UIImagePickerController!
    
    
    var auxObs: Observation!
    var measure: Measure!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        auxObs = Observation()
        valueTxtField.delegate = self
        localTxtField.delegate = self
        self.localTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.valueTxtField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard)))
        
        self.observationTxtView.delegate = self
        self.observationTxtView.text = "Escreva aqui..."
        self.observationTxtView.textColor = UIColor.lightGray
        self.observationTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.observationTxtView.layer.borderWidth = 1
        self.observationTxtView.layer.cornerRadius = 5
        self.valueTxtField.placeholder = measure.unit
        self.takeOtherPictureButton.isHidden = true
        self.imageSelectedView.isHidden = true
        self.imageSelectedView.contentMode = .scaleAspectFill
        self.imageSelectedView.layer.masksToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveObservation(_ sender: Any) {
        measure.observations.append(auxObs)
        measure.obsAdded = true
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func takeOtherPicture(_ sender: Any) {
        if (imagePicker) != nil {
            
        } else {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
        }
        
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func takePicture(_ sender: Any) {
        if (imagePicker) != nil {
            
        } else {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
        }
        
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    //MARK: - Take image
    @IBAction func addPicture(_ sender: UIButton) {
        if (imagePicker) != nil {
            
        } else {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
        }
        
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - Done image capture here
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        if let picture = info[UIImagePickerControllerOriginalImage] as? UIImage {
            auxObs.picture = picture
            self.takeOtherPictureButton.isHidden = false
            self.addPictureButton.isHidden = true
            self.imageSelectedView.image = auxObs.picture
            self.addPictureLbl.isHidden = true
            self.imageSelectedView.isHidden = false
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
}

extension MeasureObservationViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text == "" {
            textView.text = "Escreva aqui..."
            textView.textColor = UIColor.lightGray
        } else {
            self.auxObs.descricao = textView.text!
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.auxObs.descricao = textView.text!
    }
}

extension MeasureObservationViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isEqual(localTxtField) {
            auxObs.local = localTxtField.text!
        } else if textField.isEqual(valueTxtField) {
            auxObs.value = Double(valueTxtField.text!)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isEqual(localTxtField) {
            auxObs.local = localTxtField.text!
        } else if textField.isEqual(valueTxtField) {
            auxObs.value = Double(valueTxtField.text!)
        }
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if textField.isEqual(localTxtField) {
            auxObs.local = localTxtField.text!
        } else if textField.isEqual(valueTxtField) {
            auxObs.value = Double(valueTxtField.text!)
        }
    }
}
