//
//  HomeHeader.swift
//  HBC
//
//  Created by Andre Machado Parente on 17/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

protocol HomeHeaderDelegate: class {
    func didTapNewReportButton()
}

class HomeHeader: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var greetingsLbl: UILabel!
    @IBOutlet weak var numVisitsLbl: UILabel!
    @IBOutlet weak var newReportButton: UIButton!
    
    weak var delegate: HomeHeaderDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    private func commonInit() {
        Bundle.main.loadNibNamed("HomeHeader", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.newReportButton.addTarget(self, action: #selector(HomeHeader.didTapButton), for: .touchUpInside)
        self.greetingsLbl.text = "Olá, " + User.sharedInstance.name + "!"
        self.numVisitsLbl.text = "Número de visitas: " + String(User.sharedInstance.visits.count)
        self.newReportButton.titleLabel?.textColor = UIColor().customDarkGreen
        self.newReportButton.setTitleColor(UIColor().customDarkGreen, for: .normal)
        self.newReportButton.backgroundColor = .white
        self.newReportButton.layer.cornerRadius = 10
        self.newReportButton.layer.borderColor = UIColor().customDarkGreen.cgColor
        self.newReportButton.layer.borderWidth = 1
        
    }
    
    func didTapButton() {
        delegate?.didTapNewReportButton()
    }
}
