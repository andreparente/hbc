//
//  MeasureTableViewCell.swift
//  HBC
//
//  Created by Andre Machado Parente on 13/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class MeasureTableViewCell: UITableViewCell {

    @IBOutlet weak var customImage: UIImageView!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var localLbl: UILabel!
    @IBOutlet weak var observationTxtView: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.observationTxtView.layer.borderColor = UIColor.lightGray.cgColor
        self.observationTxtView.layer.borderWidth = 1
        self.observationTxtView.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
