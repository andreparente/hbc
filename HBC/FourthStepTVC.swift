//
//  FourthStepTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 11/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.

/*
 "Medição de C.M e C.E"
 "Medição de COV"
 "Umidade Relativa do Ar"
 "Sonoridade"
 "Iluminação (Lux)"
 "Microondas"
 "Teste de Fungo"
 "PH da água"
 "Bússola"
 */
//

import UIKit

class FourthStepTVC: UITableViewController {

    var room: Room!
    var measureTapped: Measure!
    var cameFromReport: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.register(UINib(nibName: "SectionHeaderDefault", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderDefault")

        if room.title == "Entrada" {
            tableView.register(UINib(nibName: "DefaultObservationTableViewCell", bundle: nil), forCellReuseIdentifier: "DefaultObservationCell")
        } else {
            if !cameFromReport {
                room.fillInitialMeasures()
            }
            
            tableView.register(UINib(nibName: "MeasureTableViewCell", bundle: nil), forCellReuseIdentifier: "MeasureCell")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let auxMeasure = measureTapped {
            if auxMeasure.obsAdded {
                print("entrou no obsAdded")
                self.tableView.reloadData()
                measureTapped.obsAdded = false
                return
            } else {
                
            }
        }
        
        if !cameFromReport {
            if (User.sharedInstance.currentVisit?.rooms?[0].obsAdded)! {
                self.tableView.reloadData()
                User.sharedInstance.currentVisit?.rooms?[0].obsAdded = false
                return
            } else {
                
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if room.title == "Entrada" {
            return 1
        } else {
            print("numero de measures ja dentro da view: ", room.measures.count)
            return room.measures.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if room.title == "Entrada" {
            return room.observations.count
        } else {
            return room.measures[section].observations.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if room.title == "Entrada" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultObservationCell", for: indexPath) as! DefaultObservationTableViewCell
            // Configure the cell...
            if let text = room.observations[indexPath.row].descricao {
                cell.descricao.text =  text
                cell.descricao.textColor = .black
            } else {
                cell.descricao.text =  "Nenhuma observação"
                cell.descricao.textColor = .lightGray
            }
            
            if let picture = room.observations[indexPath.row].picture {
                cell.picture.image = picture
                cell.picture.contentMode = .scaleAspectFill
                cell.picture.layer.masksToBounds = true
            } else {
                if cell.picture.image == nil {
                    cell.picture.downloadedFrom(link: room.observations[indexPath.row].urlPicture)
                    cell.picture.contentMode = .scaleAspectFill
                    cell.picture.layer.masksToBounds = true
                }
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MeasureCell", for: indexPath) as! MeasureTableViewCell
            // Configure the cell...
            let measure = room.measures[indexPath.section]
            if let text = measure.observations[indexPath.row].descricao {
                cell.observationTxtView.text =  text
                cell.observationTxtView.textColor = .black
            } else {
                cell.observationTxtView.text =  "Nenhuma observação"
                cell.observationTxtView.textColor = .lightGray
            }
            
            if let picture = measure.observations[indexPath.row].picture {
                cell.customImage.image = picture
                cell.customImage.contentMode = .scaleAspectFill
                cell.customImage.layer.masksToBounds = true
            } else {
                if let picture = measure.observations[indexPath.row].urlPicture {
                    if cell.customImage.image == nil {
                        cell.customImage.downloadedFrom(link: picture)
                        cell.customImage.contentMode = .scaleAspectFill
                        cell.customImage.layer.masksToBounds = true
                    }
                }
            }
            
            if let value = measure.observations[indexPath.row].value {
                cell.valueLbl.text = String(value)
            }
            
            if let local = measure.observations[indexPath.row].local {
                cell.localLbl.text = local
            }

            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionHeaderDefault") as! SectionHeaderDefault
        
        if cameFromReport {
            headerView.button.isHidden = true
        }
        
        if room.title == "Entrada" {
            if (room.observations.count) > 0 {
                headerView.title.text = "Anotações da Entrada (\((room.observations.count)))"
            } else {
                headerView.title.text = "Anotações da Entrada"
            }
            headerView.sectionNumber = section
            headerView.delegate = self
            headerView.contentView.backgroundColor = UIColor().customLightGreen
            return headerView
        } else {
            if room.measures[section].observations.count > 0 {
                headerView.title.text = "\((room.measures[section].title)!) (\(room.measures[section].observations.count))"
            } else {
                headerView.title.text = (room.measures[section].title)!
            }
            headerView.sectionNumber = section
            headerView.delegate = self
            headerView.contentView.backgroundColor = UIColor().customLightGreen
            return headerView
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50  // or whatever
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if room.title == "Entrada" {
            return 100
        } else {
            return 180
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "FourthToMeasureObservation" {
            let vc = segue.destination as! MeasureObservationViewController
            vc.measure = self.measureTapped
        } else if segue.identifier == "FourthToFifth" {
            let vc = segue.destination as! FeedbackTVC
            vc.room = self.room
        }
    }
    

}

extension FourthStepTVC: SectionHeaderDefaultDelegate {
    
    func didTapAddButton(in section: Int) {
        print("\(section)")
        if room.title == "Entrada" {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultObservation") as! DefaultObservationViewController
            vc.room = User.sharedInstance.currentVisit?.rooms?[0]
            self.present(vc, animated: true, completion: nil)
        } else {
            self.measureTapped = room.measures[section]
            self.performSegue(withIdentifier: "FourthToMeasureObservation", sender: self)
        }
    }
}
