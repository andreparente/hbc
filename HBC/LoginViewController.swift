//
//  LoginViewController.swift
//  HBC
//
//  Created by Andre Machado Parente on 16/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit
import Firebase
import SwiftyDropbox

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.keyboardHeightLayoutConstraint?.constant = 0.0
            } else {
                self.keyboardHeightLayoutConstraint?.constant = endFrame?.size.height ?? 0.0
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view.layoutIfNeeded() },
                           completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard)))
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardNotification(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.emailTxtField.delegate = self
        
     //   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Dropbox successfull"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(goToHome), name: NSNotification.Name(rawValue: "Dropbox successfull"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func goToHome() {}
    
    override func viewDidAppear(_ animated: Bool) {
        if User.sharedInstance.dropboxLogged {
            self.performSegue(withIdentifier: "LoginToHome", sender: self)
            User.sharedInstance.dropboxLogged = false
        }
    }
    
    @IBAction func login(_ sender: Any) {
        Auth.auth().signIn(withEmail: emailTxtField.text!, password: passwordTxtField.text!) { (user, error) in
            if error != nil {
                print("entrou no erro maluco")
                print((error?.localizedDescription)!)
                return
            }
            
            if user != nil {
                //user logado com sucesso
                //puxar infos do database do user
                User.sharedInstance.id = Auth.auth().currentUser?.uid
                DAO.sharedInstance.fetchUserInfo(id: User.sharedInstance.id, callback: { (success: Bool, response: String) in
                    if success {
                        print("entrou no success e nao performou segue")
                        if DropboxClientsManager.authorizedClient == nil {
                            DropboxClientsManager.authorizeFromController(UIApplication.shared,controller: self, openURL: { (url: URL) -> Void in
                                UIApplication.shared.openURL(url)
                            })
                        } else {
                            User.sharedInstance.authorizeDropbox()
                            self.performSegue(withIdentifier: "LoginToHome", sender: self)
                        }
                        
                        
                        
                    } else {
                        print("entrou no erro do callback da minha funcao")
                        print(response)
                    }
                })
            }
        }
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //LoginToHome
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(emailTxtField) {
            self.passwordTxtField.becomeFirstResponder()
        }
        return true
    }
}
