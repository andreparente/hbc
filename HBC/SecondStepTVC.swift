//
//  SecondStepTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class SecondStepTVC: UITableViewController {

    var tableHeader: EntradaHeader!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.register(UINib(nibName: "SectionHeaderDefault", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderDefault")
        tableView.register(UINib(nibName: "DefaultObservationTableViewCell", bundle: nil), forCellReuseIdentifier: "DefaultObservationCell")
        
        if User.sharedInstance.currentVisit?.rooms?.count == 0 {
            User.sharedInstance.currentVisit?.rooms?.append(Room(title: "Entrada"))
        } else if User.sharedInstance.currentVisit?.rooms?[0].title != "Entrada" {
            User.sharedInstance.currentVisit?.rooms?.insert(Room(title: "Entrada"), at: 0)
        }
        
        self.setHeaderView()
    }
    
    func setHeaderView() {
        tableHeader = EntradaHeader(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 210))
        tableHeader.titleLbl.text = User.sharedInstance.currentVisit?.rooms?[0].title
        tableHeader.descriptionTxtView.text = "Anexar fotos da entrada em que  consiga capturar as condições de iluminação e ambientação. Verificar se há elementos de comunicação psicológica com seus moradores. Adicionar o nome do cliente e seu endereço abaixo"
        tableHeader.delegate = self
        self.tableView.tableHeaderView = tableHeader

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (User.sharedInstance.currentVisit?.rooms?[0].observations.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultObservationCell", for: indexPath) as! DefaultObservationTableViewCell

        // Configure the cell...
        if let text = User.sharedInstance.currentVisit?.rooms?[0].observations[indexPath.row].descricao {
            cell.descricao.text =  text
            cell.descricao.textColor = .black
        } else {
            cell.descricao.text =  "Nenhuma observação"
            cell.descricao.textColor = .lightGray
        }
        
        if let picture = User.sharedInstance.currentVisit?.rooms?[0].observations[indexPath.row].picture {
            cell.picture.image = picture
            cell.picture.contentMode = .scaleAspectFill
            cell.picture.layer.masksToBounds = true
        }
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionHeaderDefault") as! SectionHeaderDefault
        
        if (User.sharedInstance.currentVisit?.rooms?[0].observations.count)! > 0 {
            headerView.title.text = "Anotações da Entrada (\((User.sharedInstance.currentVisit?.rooms?[0].observations.count)!))"
        } else {
            headerView.title.text = "Anotações da Entrada"
        }
        headerView.sectionNumber = section
        headerView.delegate = self
        headerView.contentView.backgroundColor = UIColor().customLightGreen
//        headerView.button.layer.cornerRadius = 0.5 * headerView.button.bounds.size.width
//        headerView.button.clipsToBounds = true
//        headerView.button.layer.borderColor = UIColor().customDarkGreen.cgColor
//        headerView.button.layer.borderWidth = 1
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50  // or whatever
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (User.sharedInstance.currentVisit?.rooms?[0].obsAdded)! {
            self.tableView.reloadData()
            User.sharedInstance.currentVisit?.rooms?[0].obsAdded = false
            return
        } else {
            
        }
    }

    @IBAction func nextStep(_ sender: Any) {
        self.performSegue(withIdentifier: "SecondToThird", sender: self)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SecondStepTVC: SectionHeaderDefaultDelegate {
    
    func didTapAddButton(in section: Int) {
        print("\(section)")
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefaultObservation") as! DefaultObservationViewController
        vc.room = User.sharedInstance.currentVisit?.rooms?[0]
        self.present(vc, animated: true, completion: nil)
    }
}

extension SecondStepTVC: EntradaHeaderDelegate {
    func addressTxtFieldChanged(text: String) {
        User.sharedInstance.currentVisit?.clientAddress = text
    }
    
    func clientNameTxtFieldChanged(text: String) {
        User.sharedInstance.currentVisit?.clientName = text
    }
}
