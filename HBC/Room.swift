//
//  Room.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation


class Room {
    var title: String!
    var measures: [Measure] = []
    var observations: [Observation] = []
    var obsAdded: Bool = false
    var feedbacks: [Observation] = []
    
    init(title: String) {
        self.title = title
    }
    
    func fillInitialMeasures() {
        self.measures.append(Measure(title: "Medição de C.M e C.E"))
        self.measures.append(Measure(title: "Medição de COV"))
        self.measures.append(Measure(title: "Umidade Relativa do Ar"))
        self.measures.append(Measure(title: "Sonoridade"))
        self.measures.append(Measure(title: "Iluminação (Lux)"))
        self.measures.append(Measure(title: "Microondas"))
        self.measures.append(Measure(title: "Teste de Fungo"))
        self.measures.append(Measure(title: "PH da água"))
        self.measures.append(Measure(title: "Bússola"))
    }
}


/*
 "Medição de C.M e C.E"
"Medição de COV"
"Umidade Relativa do Ar"
"Sonoridade"
"Iluminação (Lux)"
"Microondas"
"Teste de Fungo"
"PH da água"
"Bússola"
 */
