//
//  FeedbackTableViewCell.swift
//  HBC
//
//  Created by Andre Machado Parente on 06/11/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class FeedbackTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var feedbackText: UITextView!
    
    var feedbackTxtViewEndEditingAction: ((String)->Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.feedbackText.text = "Feedback"
        self.feedbackText.textColor = UIColor.lightGray
        self.feedbackText.delegate = self
        self.feedbackText.layer.borderColor = UIColor.lightGray.cgColor
        self.feedbackText.layer.borderWidth = 1
        self.feedbackText.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension FeedbackTableViewCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty || textView.text == "" {
            textView.text = "Feedback"
            textView.textColor = UIColor.lightGray
        } else {
            feedbackTxtViewEndEditingAction?(textView.text)
        }
    }
}
