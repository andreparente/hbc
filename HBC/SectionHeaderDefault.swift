//
//  SectionHeaderDefault.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

protocol SectionHeaderDefaultDelegate: class {
    func didTapAddButton(in section: Int)
}

class SectionHeaderDefault: UITableViewHeaderFooterView {
    
    weak var delegate: SectionHeaderDefaultDelegate?
    @IBOutlet weak var title: UILabel!
    var sectionNumber: Int!
    
    @IBOutlet weak var button: UIButton!
    @IBAction func didTapAddButton(_ sender: UIButton) {
        delegate?.didTapAddButton(in: sectionNumber)
    }

}


