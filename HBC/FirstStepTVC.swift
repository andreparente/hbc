//
//  FirstStepTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 09/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//------------------------------------------------------------ TELA DO CHECKLIST DOS EQUIPAMENTOS ---------------------------------------
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------

import UIKit

class FirstStepTVC: UITableViewController {

    var tableHeader: HeaderDefault!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSForegroundColorAttributeName: UIColor().customDarkGreen,
             NSFontAttributeName: UIFont.systemFont(ofSize: 18, weight: UIFontWeightSemibold)]
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.setHeaderView()
        self.tableView.register(UINib(nibName: "CheckListTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckListCell")
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(FirstStepTVC.dismissKeyboard)))
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    func setHeaderView() {
        tableHeader = HeaderDefault(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
        tableHeader.title.text = "Checklist dos equipamentos"
        tableHeader.text.text = "Fazer um check-list de todos os instrumentos que serão utilizados com o cliente, verificar a bateria dos aparelhos, limpeza e organização da maleta. O aparelho de celular deve estar com bateria suficiente para executar as fotos necessárias para registro das medições."
        self.tableView.tableHeaderView = tableHeader
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (User.sharedInstance.currentVisit?.equipments?.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListCell", for: indexPath) as! CheckListTableViewCell

        // Configure the cell...
        cell.titleLbl.text = User.sharedInstance.currentVisit?.equipments?[indexPath.row].title
        
        
        if (User.sharedInstance.currentVisit?.equipments?[indexPath.row].state)! {
            cell.okNotOkSwitch.isOn = true
            cell.okNotOKLbl.text = "Ok"
        } else {
            cell.okNotOkSwitch.isOn = false
            cell.okNotOKLbl.text = "Not Ok"
        }
        
        // set a "Callback Closure" in the cell
        cell.switchTapAction = {
            (isOn) in
            // update our Data Array to the new state of the switch in the cell
            User.sharedInstance.currentVisit?.equipments?[indexPath.row].state = isOn
        }
        
        cell.observacaoTxtViewEndEditingAction = {
            (observacao) in
            if observacao == "Observação" || observacao == "" {
                User.sharedInstance.currentVisit?.equipments?[indexPath.row].observation = ""
            } else {
                User.sharedInstance.currentVisit?.equipments?[indexPath.row].observation = observacao
            }
        }
        
        if User.sharedInstance.currentVisit?.equipments?[indexPath.row].observation == "" {
            cell.observacaoTxtView.text = "Observação"
            cell.observacaoTxtView.textColor = .lightGray
        } else {
            cell.observacaoTxtView.text = User.sharedInstance.currentVisit?.equipments?[indexPath.row].observation
            cell.observacaoTxtView.textColor = .black
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    @IBAction func dismissVC(_ sender: Any) {
        User.sharedInstance.currentVisit?.removeValues()
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func nextStep(_ sender: Any) {
        
        //verificar algo antes de ir pro proximo passo?
        self.performSegue(withIdentifier: "FirstToSecond", sender: self)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
