//
//  Consulta.swift
//  HBC
//
//  Created by Andre Machado Parente on 09/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation


class Visit {
    
    var equipments: [Equipment]? = []
    var rooms: [Room]? = []
    var clientName: String?
    var clientAddress: String?
    var time: String?
    var pdf: Data?
    var pdfUrlString: String!
    var id: String!
    
    init() {
        self.equipments = [Equipment(title: "Uniforme"), Equipment(title: "Credencial"), Equipment(title: "Formulários"),Equipment(title: "Medidor de COV"), Equipment(title: "Medidor de Radioatividade"), Equipment(title: "Medidor Multímetro"),Equipment(title: "Medidor de Micro Ondas"), Equipment(title: "Medidor de C.E e C.M"), Equipment(title: "Teste de Fungo"), Equipment(title: "Equipamento de Segurança"), Equipment(title: "Caneta Detectora de C.E"), Equipment(title: "Bússola"), Equipment(title: "Aparelhos com bateria 100%"), Equipment(title: "Celular com bateria 100%")]
    }
    
    init(withEquipments: Bool) {
        
    }
    
    func hasRoomWithEmptyObs() -> Bool {
        for room in self.rooms! {
            
            if room.observations.count > 0 {
                return false
            }
            
            for measure in room.measures {
                if measure.observations.count > 0 {
                    return false
                }
            }
        }
        return true
    }
    
    init(dict: [String : Any]) {
        self.clientName = dict["clientName"] as? String
        self.clientAddress = dict["clientAddress"] as? String
        self.time = dict["visitTime"] as? String
        
        //pdf
        if let pdfDict = dict["pdfs"] as? [String: Any] {
            for pdf in pdfDict {
            //    self.pdf = NSData(contentsOf: URL(string: pdf.value as! String)!)! as Data
                self.pdfUrlString = pdf.value as! String
            }
        }
        
        //equipments
        let equipmentsDict = dict["equipments"] as! [String : Any]
        for equipmentDict in equipmentsDict {
            let equipDictAux = equipmentDict.value as! [String : Any]
            let equipAux: Equipment = Equipment(title: equipDictAux["title"] as! String)
            equipAux.state = equipDictAux["state"] as! Bool
            equipAux.observation = equipDictAux["observation"] as! String
            self.equipments?.append(equipAux)
        }
        
        //comodos
        let comodosDict = dict["rooms"] as! [String : Any]
        
        for comodoDict in comodosDict {
            
            //Entrada
            if comodoDict.key == "Entrada" {
                if self.rooms?.count == 0 {
                    self.rooms?.append(Room(title: "Entrada"))
                } else {
                    self.rooms?.insert(Room(title: "Entrada"), at: 0)
                }
                
                print(comodoDict.value)
                
                let aux = comodoDict.value as! [String : Any]
                let observations = aux["generalObs"] as! [String : Any]
                for obs in observations {
                    let aux = obs.value as! [String : Any]
                    let obsAux = Observation(descricao: aux["description"] as! String, value: aux["value"] as! Double, urlPicture: aux["url"] as! String, local: aux["local"] as! String)
                    obsAux.id = obs.key
                    self.rooms?[0].observations.append(obsAux)
                }
                
//                if let feedbacks = aux["feedbacks"] as? [String : Any] {
//                    for obs in feedbacks {
//                        let aux = obs.value as! [String : Any]
//                        let obsAux = Observation()
//                        obsAux.id = obs.key
//                        obsAux.descricao = aux["description"] as! String
//                        obsAux.urlPicture = aux["url"] as! String
//                        self.rooms?[0].feedbacks.append(obsAux)
//                    }
//                }
                
            } else {
                
                //Outros Cômodos
                let roomAux = Room(title: comodoDict.key)
                let roomDict = comodoDict.value as! [String : Any]
                for roomAuxDict in roomDict {
                    if roomAuxDict.key == "feedbacks" {
                        let feedbacksAuxDict = roomAuxDict.value as! [String:Any]
                        for feedbackDict in feedbacksAuxDict {
                            let feedbackAux = Observation()
                            feedbackAux.id = feedbackDict.key
                            let aux = feedbackDict.value as! [String: Any]
                            feedbackAux.descricao = aux["description"] as! String
                            feedbackAux.urlPicture = aux["url"] as! String
                            roomAux.feedbacks.append(feedbackAux)
                        }
                        
                    } else if roomAuxDict.key == "measures" {
                        let meaDictAux = roomAuxDict.value as! [String : Any]
                        for meaDict in meaDictAux {
                            let aux = meaDict.value as! [String : Any]
                            let measureAux = Measure(title: aux["title"] as! String)
                            
                            if let observations = aux["observations"] as? [String : Any] {
                                for obs in observations {
                                    let aux = obs.value as! [String : Any]
                                    let obsAux = Observation(descricao: aux["description"] as! String, value: aux["value"] as! Double, urlPicture: aux["url"] as! String, local: aux["local"] as! String)
                                    obsAux.id = obs.key
                                    measureAux.observations.append(obsAux)
                                }
                            }
                            roomAux.measures.append(measureAux)
                        }
                    }
                }
                self.rooms?.append(roomAux)
            }
        }
    }
    
    
    func printState() {
        print("-------------------------------------------------------")
        print("EQUIPAMENTOS DA VISITA: ")
        for equipment in equipments! {
            print("Nome do equipamento: ", equipment.title)
            print("----------------------")
            print("estado do equipamento: ", equipment.state)
            print("----------------------")
            print("OBS do equipamento: ", equipment.observation)
        }
        
        print("-------------------------------------------------------")
        print("COMODOS DA VISITA: ")
        for comodo in rooms! {
            print("Nome do comodo: ", comodo.title)
            print("----------------------------------")
            if comodo.observations.count > 0 {
                for obs in comodo.observations {
                    print("observacao do comodo geral: ", obs.descricao)
                    print("----------------------")
                }
            }
            if comodo.measures.count > 0 {
                for medicao in comodo.measures {
                    print("nome da medicao: ", medicao.title)
                    print("unidade de medida: ", medicao.unit)
                    for obs in medicao.observations {
                        print("observacao da medicao: ", obs.descricao)
                        print("valor da medicao: ", obs.value)
                        print("----------------------")
                    }
                }
            }
        }
    }
    
    
    func transformIntoDictionary(date: String) -> [String : Any] {
        
        var reportDict: [String : Any] = Dictionary()
        var equipmentDict: [String : Any] = Dictionary()
        var measuresDict: [String : Any] = Dictionary()
        var measureAuxDict: [String : Any] = Dictionary()
        var countEquipment = 1
        for equipment in equipments! {
            equipmentDict["equipment"+String(countEquipment)] = ["title" : equipment.title, "state" : equipment.state, "observation" : equipment.observation]
            countEquipment += 1
        }
        var roomDict: [String : Any] = Dictionary()

        for comodo in rooms! {

            if comodo.observations.count > 0 {
                var count = 1
                var obsDict: [String : Any] = Dictionary()
                for obs in comodo.observations {
                    obsDict[obs.id] = ["description":obs.descricao, "value":obs.value, "url":obs.urlPicture, "local":obs.local]
                    count += 1
                }
                roomDict[comodo.title] = ["generalObs" : obsDict]
            }
            
            if comodo.measures.count > 0 {
                
                var countMedicao = 1
                for medicao in comodo.measures {
                    var obs2Dict: [String : Any] = Dictionary()
                    var count = 1
                    for obs in medicao.observations {
                        obs2Dict[obs.id] = ["description":obs.descricao, "value":obs.value, "url":obs.urlPicture, "local":obs.local]
                        count += 1
                    }
                    measureAuxDict["measure" + String(countMedicao)] = ["title" : medicao.title, "observations" : obs2Dict]
                    countMedicao += 1
                }
                measuresDict["measures"] = measureAuxDict
                roomDict[comodo.title] = measuresDict
            }
        }
        reportDict["clientName"] = self.clientName
        reportDict["clientAddress"] = self.clientAddress
        reportDict["visitTime"] = date
        reportDict["rooms"] = roomDict
        reportDict["equipments"] = equipmentDict
        return reportDict
    }
    
    func removeValues() {
        self.equipments = nil
        self.rooms = nil
        self.clientName = nil
        self.clientAddress = nil
        self.time = nil
        self.pdf = nil
    }
}
