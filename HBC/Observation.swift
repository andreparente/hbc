//
//  Observation.swift
//  HBC
//
//  Created by Andre Machado Parente on 10/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation
import UIKit

class Observation {
    var descricao: String!
    var value: Double!
    var urlPicture: String!
    var picture: UIImage!
    var local: String!
    var id: String!
    
    
    init() {
        self.local = ""
        self.urlPicture = ""
        self.value = 0
        self.descricao = ""
    }
    
    init(descricao: String, value: Double, urlPicture: String, local: String) {
        self.descricao = descricao
        self.value = value
        self.urlPicture = urlPicture
        self.local = local
    }
}

class Feedback: Observation {
    
    override init() {
        super.init()
    }
}
