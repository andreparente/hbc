//
//  Equipment.swift
//  HBC
//
//  Created by Andre Machado Parente on 09/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation

class Equipment {
    var title: String!
    var observation: String!
    var state: Bool!
    
    init(title: String) {
        self.state = false
        self.title = title
        self.observation = ""
    }
}
