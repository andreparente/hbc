//
//  DAO.swift
//  HBC
//
//  Created by Andre Machado Parente on 16/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import Firebase
import FirebaseStorage
import SwiftyDropbox

class DAO {
    
    
    var usersRef: DatabaseReference?
    var reportsRef: DatabaseReference?
    
    var storageRef: StorageReference?
    
    var reportPath: String!

    
    //Singleton!
    static let sharedInstance = DAO()
    
    private init() {
        usersRef = Database.database().reference()
        usersRef = usersRef?.child("users")
        
        reportsRef = Database.database().reference()
        reportsRef = reportsRef?.child("reports")
        
        let storage = Storage.storage()
        self.storageRef = storage.reference()

    }
    
    func fetchUserInfo(id: String, callback: @escaping ((_ success: Bool, _ response: String)->())) {
        print("entrou na minha funcao fetchuserinfo")
        print(id)
        usersRef?.child(id).observeSingleEvent(of: .value, with: { (snapshot: DataSnapshot) in
            let userDict = snapshot.value as? [String: Any] ?? [:]
            if !userDict.isEmpty {
                
                //preencher as infos no user local
                User.sharedInstance.name = userDict["name"] as! String
                User.sharedInstance.email = userDict["email"] as! String
                User.sharedInstance.id = id
                
                //TODO FETCH friendsID/favoriteArts/favoriteArtists
                
                callback(true, "")
            } else {
                callback(false, "Erro")
            }
        }, withCancel: { (error: Error) in
            callback(false, error.localizedDescription)
        })
    }
    
    func saveImageFor(id: String, room: Room, obs:Observation, callback: @escaping((_ success: Bool, _ response: String)->())) {
        
        let dataToUpload = UIImageJPEGRepresentation(obs.picture, 0.5)
        
        storageRef = DAO.sharedInstance.storageRef!
        print(storageRef ?? 0)
        var ref: StorageReference?
        
        
        ref = storageRef?.child(User.sharedInstance.id).child(id).child(room.title).child("observations").child("\(obs.id)" + ".jpg")
        
        ref?.putData(dataToUpload!, metadata: nil, completion: {(metadata, error) in
            if error != nil{
                print (error ?? 0)
                callback(false,(error?.localizedDescription)!)
                return
            }
            else{
                print(metadata ?? 0)
                
                obs.urlPicture = metadata?.downloadURL()?.absoluteString
                
                
                callback(true,"True")
            }
        })
    }
    

    
    func saveReportPictures(report: Visit, id: String, callback: @escaping ((_ success: Bool, _ response: String)->())) {
        var totalOfImages = 0
        
        for room in report.rooms! {
            for obs in room.observations {
                if obs.picture != nil {
                    totalOfImages += 1
                }
            }
            
            for measure in room.measures {
                for obs in measure.observations {
                    if obs.picture != nil {
                        totalOfImages += 1
                    }
                }
            }
        }
        
        var count = 0
        for room in report.rooms! {
            var indexRoom = 1
            for obs in room.observations {
                if obs.picture != nil {
                    
                    self.saveImageFor(id: id, room: room, obs: obs, callback: { (success: Bool, response: String) in
                        if success {
                            count += 1
                            if count == totalOfImages {
                                callback(true,"Todas imagens carregadas")
                            }
                        } else {
                            callback(false, "erro ao salvar pelo menos uma imagem")
                        }
                    })
                    indexRoom += 1
                } else {
                    if count == totalOfImages {
                        callback(true,"Todas imagens carregadas")
                    } else {
                        callback(false, "erro ao salvar pelo menos uma imagem")
                    }
                }
            }
        
            for measure in room.measures {
                var index = 1
                for obs in measure.observations {
                    if obs.picture != nil {
                        self.saveImageFor(id: id, room: room, obs: obs, callback: { (success: Bool, response: String) in
                            if success {
                                count += 1
                                if count == totalOfImages {
                                    callback(true,"Todas imagens carregadas")
                                }
                            } else {
                                callback(false, "erro ao salvar pelo menos uma imagem")
                            }
                        })
                        index += 1
                    }else {
                        if count == totalOfImages {
                            callback(true,"Todas imagens carregadas")
                        } else {
                            callback(false, "erro ao salvar pelo menos uma imagem")
                        }
                    }
                }
            }
        }
    }
    
    func saveReport(report: Visit, callback: @escaping ((_ success: Bool, _ response: String)->())) {

        let date = Date().description
        let id = User.sharedInstance.id + date
        
        if (report.rooms?.count)! <= 1 {
            callback(false, "Adicione algum cômodo além da Entrada")
            return
        } else if report.hasRoomWithEmptyObs() {
            callback(false, "Adicione ao menos uma observação em cada Cômodo")
            return
        }

        self.saveReportPictures(report: report, id: id) { (success: Bool, response: String) in
            if success {
                self.usersRef?.child(User.sharedInstance.id).child("reports").child(id).setValue(report.transformIntoDictionary(date: date), withCompletionBlock: { (error: Error?, ref: DatabaseReference) in
                    if error == nil {
                        User.sharedInstance.visits.append(report)
                        callback(true, "deu certo")
                    } else {
                        callback(false, "deu ruim")
                        print(error?.localizedDescription ?? "")
                    }
                })
            } else {
                callback(false, response)

            }
        }
    }
    
    func saveReportPDF(report: Visit, callback: @escaping ((_ success: Bool, _ response: String)->()))  {
        
        storageRef?.child("pdfs").child(report.clientName! + ".pdf").putData(report.pdf!, metadata: nil, completion: { (metadata: StorageMetadata?, error: Error?) in
            if error != nil {
                callback(false,(error?.localizedDescription)!)
            } else {
                let urlPDF = metadata?.downloadURL()?.absoluteString
                self.usersRef?.child(User.sharedInstance.id).child("reports").child(report.id).child("pdfs").child(Date().description).setValue(urlPDF, withCompletionBlock: { (error: Error?, ref: DatabaseReference) in
                    if error != nil {
                        callback(false,(error?.localizedDescription)!)
                    } else {
                        callback(true,"DEU CERTO PORRA")
                    }
                })
            }
        })
        
    }
    
    func saveFeedbackFor(room: Room, report: Visit, callback: @escaping ((_ success: Bool, _ response: String)->())) {
        
        let date = Date().description
        let id = User.sharedInstance.id + date
        
        self.saveFeedbacksPictures(room: room, report: report, id: id) { (success: Bool, response: String) in
            if success {
                var obsDict: [String : Any] = Dictionary()
                for feedback in room.feedbacks {
                    obsDict[feedback.id] = ["description":feedback.descricao, "url":feedback.urlPicture]
                }
                self.usersRef?.child(User.sharedInstance.id).child("reports").child(report.id).child("rooms").child(room.title).child("feedbacks").setValue(obsDict, withCompletionBlock: { (error: Error?, ref: DatabaseReference) in
                    if error != nil {
                        print(error?.localizedDescription)
                        callback(false,"")
                    } else {
                        callback(true,"")
                    }
                })
            } else {
                callback(false,response)
            }
        }
    
    }
    
    func saveFeedbacksPictures(room: Room, report: Visit, id: String, callback: @escaping ((_ success: Bool, _ response: String)->())) {
        var totalOfImages = 0
        for feedback in room.feedbacks {
            if feedback.picture != nil {
                totalOfImages += 1
            }
        }
        
        var count = 0
        var index = 1
        for feedback in room.feedbacks {
            if feedback.picture != nil {
                self.saveImageForFeedback(id: id, room: room, obs: feedback, callback: { (success: Bool, response: String) in
                    if success {
                        count += 1
                        if count == totalOfImages {
                            callback(true,"Todas imagens carregadas")
                        }
                    } else {
                        callback(false, "erro ao salvar pelo menos uma imagem")
                    }
                })
                index += 1
            } else {
                if count == totalOfImages {
                    callback(true,"Todas imagens carregadas")
                } else {
                }
            }
        }

    }
    
    func saveImageForFeedback(id: String, room: Room, obs:Observation, callback: @escaping((_ success: Bool, _ response: String)->())) {
        
        let dataToUpload = UIImageJPEGRepresentation(obs.picture, 0.5)
        
        storageRef = DAO.sharedInstance.storageRef!
        print(storageRef ?? 0)
        var ref: StorageReference?
        let date = Date()
        
        
        ref = storageRef?.child(User.sharedInstance.id).child(id).child(room.title).child("feedbacks").child("\(obs.id)" + ".jpg")
        
        ref?.putData(dataToUpload!, metadata: nil, completion: {(metadata, error) in
            if error != nil{
                print (error ?? 0)
                callback(false,(error?.localizedDescription)!)
                return
            }
            else{
                print(metadata ?? 0)
                
                obs.urlPicture = metadata?.downloadURL()?.absoluteString
                
                
                callback(true,"True")
            }
        })
    }
    
    
    func fetchReports(callback: @escaping ((_ success: Bool, _ response: String)->())) {
        
        User.sharedInstance.visits.removeAll()
        usersRef?.child(User.sharedInstance.id).child("reports").queryOrderedByKey().observeSingleEvent(of: .value, with: { (snapshot: DataSnapshot) in
            let reportsDict = snapshot.value as? [String : Any] ?? [:]
            if reportsDict.isEmpty {
                callback(true,"Nenuma visita")

            } else {
                for report in reportsDict {
                    let dict = report.value as! [String : Any]
                    let reportAux: Visit = Visit(dict: dict)
                    reportAux.id = report.key
                    User.sharedInstance.visits.append(reportAux)
                }
                callback(true,"deu certo!")
            }
            
        }, withCancel: { (error: Error) in
            print(error.localizedDescription)
            callback(false,"deu ruim.....")
        })
    }
    
    
    //---------------- MARK : DROPBOX
    
    func sendReportToDropBox(report: Visit) {
        
        reportPath = "/HBC/\(User.sharedInstance.name.folding(options: .diacriticInsensitive, locale: .current))/\((report.clientName)!)"
        //
        print(reportPath)
        
        User.sharedInstance.dropBoxClient?.files.createFolderV2(path: reportPath).response(completionHandler: { (files: (Files.CreateFolderResult)?, error: CallError<(Files.CreateFolderError)>?) in
            if error != nil{
                switch error! {
                case .routeError( _, let string, let string1, let string2):
                    print("erro na rota!")
                    print(string ?? 0)
                    print(string1 ?? 0)
                    print(string2 ?? 0)
                    break
                default:
                    break
                }
            } else {
                //supondo que é a primeira vez e vai conseguir criar de boa
                print(files ?? 0)
                if let data = self.createTxtEquipmentFile(title: "", text: "") {
                    
                    User.sharedInstance.dropBoxClient?.files.upload(path: self.reportPath + "/Equipamentos.txt", input: data).response(completionHandler: { (files: (Files.FileMetadata)?, error: CallError<(Files.UploadError)>?) in
                        if let error = error {
                            print(error)
                        } else {
                            print(files ?? 0)
                        }
                    })
                }
            }
        })
        
        var count = 0
        for room in report.rooms! {
            
            User.sharedInstance.dropBoxClient?.files.createFolderV2(path: self.reportPath + "/\(room.title.folding(options: .diacriticInsensitive, locale: .current))").response(completionHandler: { (files: (Files.CreateFolderResult)?, error: CallError<(Files.CreateFolderError)>?) in
                if let error = error {
                    print(error)
                } else {
                    //deu certo
                    count += 1
                    if(count == self.report.rooms?.count) {
                        print("ENTROU NO IF DO COUNT, OU SEJA, CRIOU TODAS AS PASTAS DOS COMODOS E AGORA VAI POPULAR")
                        self.saveObservationForEachRoom(callback: { (success: Bool) in
                            if success {
                                print("DEU BOM!!!!!!!!!!!!")
                            } else {
                                print("DEU RUIM")
                            }
                        })
                    }
                }
            })
            
        }
    }
    
    func saveObservationForEachRoom(callback: @escaping((_ success: Bool)->())) {
        for room in report.rooms! {
            if room.observations.count > 0 {
                for obs in room.observations {
                    if let txtData = self.createEntradaTxtFile(title: obs.id!, obs: obs) {
                        
                        if let url = URL(string: obs.urlPicture) {
                            do {
                                let pictureData = try Data(contentsOf: url)
                                let uiimage = UIImage(data: pictureData)
                                let imageData = UIImageJPEGRepresentation(uiimage!, 0.5)
                                
                                //Foto da OBS
                                User.sharedInstance.dropBoxClient?.files.upload(path: self.reportPath + "/\(room.title.folding(options: .diacriticInsensitive, locale: .current))/\(obs.id!)/Foto.jpeg", mode: .overwrite, autorename: false, clientModified: nil, mute: false, propertyGroups: nil, input: imageData!).response(completionHandler: { (files: (Files.FileMetadata)?, error: CallError<(Files.UploadError)>?) in
                                    if let error = error {
                                        print(error)
                                    } else {
                                        
                                        //Observacao txt da OBS
                                        User.sharedInstance.dropBoxClient?.files.upload(path: self.reportPath + "/\(room.title.folding(options: .diacriticInsensitive, locale: .current))/\(obs.id!)/Observation.txt", mode: Files.WriteMode.overwrite, autorename: false, clientModified: nil, mute: false, propertyGroups: nil, input: txtData).response(completionHandler: { (files: (Files.FileMetadata)?, error: CallError<(Files.UploadError)>?) in
                                            if let error = error {
                                                print(error)
                                            } else {
                                                print(files ?? 0)
                                            }
                                        })
                                    }
                                })
                            } catch {
                                
                            }
                        }
                    }
                }
            } else {
                //outros comodos além da entrada
                for measure in room.measures {
                    print("nome da medicao: ", measure.title)
                    for obs in measure.observations {
                        print("observacao da medicao: ", obs.descricao)
                        print("valor da medicao: ", obs.value)
                        if let url = URL(string: obs.urlPicture) {
                            do {
                                let pictureData = try Data(contentsOf: url)
                                let uiimage = UIImage(data: pictureData)
                                let imageData = UIImageJPEGRepresentation(uiimage!, 0.5)
                                
                                print(imageData!)
                                print(obs.id!)
                                
                                //salvando a FOTO da obs
                                User.sharedInstance.dropBoxClient?.files.upload(path: self.reportPath + "/\(room.title.folding(options: .diacriticInsensitive, locale: .current))/\(measure.title.folding(options: .diacriticInsensitive, locale: .current))/\(obs.id!)/Foto.jpeg", mode: .overwrite, autorename: false, clientModified: nil, mute: false, propertyGroups: nil, input: imageData!).response(completionHandler: { (files: (Files.FileMetadata)?, error: CallError<(Files.UploadError)>?) in
                                    if let error = error {
                                        print(error)
                                    } else {
                                        print(files ?? 0)
                                        
                                        //salvando o texto/infos sobre a foto
                                        if let txtData = self.createObservationTxtFile(obs: obs) {
                                            User.sharedInstance.dropBoxClient?.files.upload(path: self.reportPath + "/\(room.title.folding(options: .diacriticInsensitive, locale: .current))/\(measure.title.folding(options: .diacriticInsensitive, locale: .current))/\(obs.id!)/Observation.txt", mode: Files.WriteMode.overwrite, autorename: false, clientModified: nil, mute: false, propertyGroups: nil, input: txtData).response(completionHandler: { (files: (Files.FileMetadata)?, error: CallError<(Files.UploadError)>?) in
                                                if let error = error {
                                                    print(error)
                                                } else {
                                                    print(files ?? 0)
                                                }
                                            })
                                        }
                                        
                                    }
                                })
                            } catch {
                                
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    func createObservationTxtFile(obs: Observation) -> Data? {
        var dataToReturn = Data()
        let text = "Observação: \(obs.descricao!) \n\n" + "Valor: \(obs.value!) \n\n" + "Local: \(obs.local!) \n\n"
        dataToReturn = text.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        return dataToReturn
    }
    
    func createTxtEquipmentFile(title: String, text: String) -> Data? {
        var dataToReturn = Data()
        
        let file = "Equipments.txt" //this is the file. we will write to and read from it
        var text: String = ""
        
        for equip in report.equipments! {
            text += "Nome: \(equip.title!) \nEstado: \(equip.state!) \nObservação: \(equip.observation!) \n\n"
        }
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {/* error handling here */}
            
            //reading
            do {
                _ = try String(contentsOf: fileURL, encoding: .utf8)
                dataToReturn = try Data(contentsOf: fileURL)
            }
            catch {/* error handling here */}
            return dataToReturn
        } else {
            return nil
        }
    }
    
    func createEntradaTxtFile(title: String, obs: Observation) -> Data? {
        var dataToReturn = Data()
        
        let file = ".txt" //this is the file. we will write to and read from it
        var text: String = ""
        
        text += "Descrição: \(obs.descricao!) \n\n"
        text += "Local: \(obs.local!) \n\n"
        
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileURL = dir.appendingPathComponent(file)
            
            //writing
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {/* error handling here */}
            
            //reading
            do {
                _ = try String(contentsOf: fileURL, encoding: .utf8)
                dataToReturn = try Data(contentsOf: fileURL)
            }
            catch {/* error handling here */}
            return dataToReturn
        } else {
            return nil
        }
    }
}
