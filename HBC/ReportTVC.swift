//
//  ReportTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 01/11/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit
import MessageUI
import PDFGenerator
import SwiftyDropbox

class ReportTVC: UITableViewController {
    
    var report: Visit!
    var indexSelected: Int = 0
    var mailComposer = MFMailComposeViewController()
    var webView: UIWebView!
    

    private enum MIMEType: String {
        case jpg = "image/jpeg"
        case png = "image/png"
        case doc = "application/msword"
        case ppt = "application/vnd.ms-powerpoint"
        case html = "text/html"
        case pdf = "application/pdf"
        
        init?(type: String) {
            switch type.lowercased() {
            case "jpg": self = .jpg
            case "png": self = .png
            case "doc": self = .doc
            case "ppt": self = .ppt
            case "html": self = .html
            case "pdf": self = .pdf
            default: return nil
            }
        }
    }
    
    
    @IBOutlet weak var segControl: UISegmentedControl!
    
    
    
    private func showMailComposerWith(attachmentName: String, data: Data?) {
        
        if MFMailComposeViewController.canSendMail() {
            
            let subject = "[Teste] Relatório HBC"
            let messageBody = "Caro, " + report.clientName! + ", segue o relatório em anexo, qualquer dúvida entrar em contato!"
            let toRecipients = ["andremachadoparente@gmail.com","luciano74alves@gmail.com"]
            
            mailComposer.mailComposeDelegate = self
            mailComposer.setSubject(subject)
            mailComposer.setToRecipients(toRecipients)
            mailComposer.setMessageBody(messageBody, isHTML: false)
            let fileParts = attachmentName.components(separatedBy: ".")
            let fileName = fileParts[0]
            let fileExtension = fileParts[1]
            
            //esse código funciona guardando no celular os relatórios
            //            let fm = FileManager.default
            //            let docsurl = try! fm.url(for:.documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            //            let myurl = docsurl.appendingPathComponent(attachmentName)
            //          //  NSData(contentsOf: myurl)
            //            if let fileData = NSData(contentsOf: myurl), let mimeType = MIMEType(type: fileExtension) {
            //                mailComposer.addAttachmentData(fileData as Data, mimeType: mimeType.rawValue, fileName: fileName)
            //                self.present(mailComposer, animated: true, completion: nil)
            //            } else {
            //                print("erro no primeiro fileData")
            //            }
            
            //esse aqui é criando o arquivo pdf e nao armazenar
            if let fileData = data, let mimeType = MIMEType(type: fileExtension) {
                mailComposer.addAttachmentData(fileData as Data, mimeType: mimeType.rawValue, fileName: fileName)
                self.present(mailComposer, animated: true, completion: nil)
            } else {
                print("erro no primeiro fileData")
            }
        } else {
            let vc = UIActivityViewController(activityItems: [data], applicationActivities: nil)
            self.present(vc, animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if report.pdf != nil {
            segControl.insertSegment(withTitle: "pdf", at: 2, animated: true)
            webView = UIWebView(frame: self.view.frame)
            self.webView.isHidden = true
            webView.loadRequest(URLRequest(url: URL(string: report.pdfUrlString)!))
            self.view.addSubview(webView)
            
        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.tableView.register(UINib(nibName: "CheckListTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckListCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func segChanged(_ sender: Any) {
        if segControl.selectedSegmentIndex == 2 {
            self.webView.isHidden = false
            self.view.bringSubview(toFront: webView)
        } else {
            
            //   self.webView.isHidden = true
            self.view.bringSubview(toFront: tableView)
            self.tableView.reloadData()
        }
    }
    
    @IBAction func sendEmail(_ sender: Any) {
        //salvar as pastas e imagens
        
        

    }
    


    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if segControl.selectedSegmentIndex == 0 {
            return 1
        } else {
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if segControl.selectedSegmentIndex == 0 {
            return (report.equipments?.count)!
        } else {
            return (report.rooms?.count)!
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if segControl.selectedSegmentIndex == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListCell", for: indexPath) as! CheckListTableViewCell
            
            // Configure the cell...
            cell.titleLbl.text = report.equipments?[indexPath.row].title
            
            
            if (report.equipments?[indexPath.row].state)! {
                cell.okNotOkSwitch.isOn = true
                cell.okNotOKLbl.text = "Ok"
            } else {
                cell.okNotOkSwitch.isOn = false
                cell.okNotOKLbl.text = "Not Ok"
            }
            cell.okNotOkSwitch.isHidden = true
            cell.observacaoTxtView.isEditable = false
            
            cell.observacaoTxtViewEndEditingAction = {
                (observacao) in
                if observacao == "Observação" || observacao == "" {
                    self.report.equipments?[indexPath.row].observation = ""
                } else {
                    self.report.equipments?[indexPath.row].observation = observacao
                }
            }
            
            if report.equipments?[indexPath.row].observation == "" {
                cell.observacaoTxtView.text = "Observação"
                cell.observacaoTxtView.textColor = .lightGray
            } else {
                cell.observacaoTxtView.text = report.equipments?[indexPath.row].observation
                cell.observacaoTxtView.textColor = .black
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            cell.textLabel?.text = report.rooms?[indexPath.row].title
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if segControl.selectedSegmentIndex == 0 {
            return 120
        } else {
            return 44
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if segControl.selectedSegmentIndex == 0 {
        } else {
            self.indexSelected = indexPath.row
            self.performSegue(withIdentifier: "ReportToMeasures", sender: self)
        }
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ReportToMeasures" {
            let dest = segue.destination as! FourthStepTVC
            dest.cameFromReport = true
            dest.room = report.rooms?[indexSelected]
        }
    }
    
    
}


extension ReportTVC: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case MFMailComposeResult.cancelled:
            controller.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.failed:
            controller.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.saved:
            controller.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.sent:
            controller.dismiss(animated: true, completion: nil)
        default:
            controller.dismiss(animated: true, completion: nil)
        }
    }
    
}
