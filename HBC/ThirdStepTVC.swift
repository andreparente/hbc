//
//  RoomsTVC.swift
//  HBC
//
//  Created by Andre Machado Parente on 11/10/17.
//  Copyright © 2017 Andre Machado Parente. All rights reserved.
//

import UIKit

class ThirdStepTVC: UITableViewController {

    var tableHeader: HeaderDefault!
    var roomSelected: Room!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.register(UINib(nibName: "SectionHeaderDefault", bundle: nil), forHeaderFooterViewReuseIdentifier: "SectionHeaderDefault")

        self.setHeaderView()
    }

    func setHeaderView() {
        tableHeader = HeaderDefault(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 150))
        tableHeader.title.text = "Cômodos"
        tableHeader.text.text = "Agora você irá adicionar os cômodos, para cada cômodo, você pode clicar nele e preencher suas medições!"
        self.tableView.tableHeaderView = tableHeader
        
    }
    
    @IBAction func saveReport(_ sender: Any) {
        
        DAO.sharedInstance.saveReport(report: (User.sharedInstance.currentVisit)!) { (success: Bool, response: String) in
            
            if success {

                //1. Create the alert controller.
                let alert = UIAlertController(title: "Report Saved Successfully", message: "", preferredStyle: .alert)

                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { [weak alert] (_) in
                        self.navigationController?.popToRootViewController(animated: true)
                }))
                
                // 4. Present the alert.
                self.present(alert, animated: true, completion: nil)
            } else {
                
            }
        }
        //DAO.sharedInstance.saveReport(report: User.sharedInstance.currentVisit?, callback: ((Bool, String) -> ()))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return (User.sharedInstance.currentVisit?.rooms?.count)!
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = User.sharedInstance.currentVisit?.rooms?[indexPath.row].title
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionHeaderDefault") as! SectionHeaderDefault
        
        if (User.sharedInstance.currentVisit?.rooms?.count)! > 0 {
            headerView.title.text = "Casa (\((User.sharedInstance.currentVisit?.rooms?.count)!))"
        } else {
            headerView.title.text = "Casa"
        }
        headerView.sectionNumber = section
        headerView.delegate = self
        headerView.contentView.backgroundColor = UIColor().customLightGreen
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50  // or whatever
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.roomSelected = User.sharedInstance.currentVisit?.rooms?[indexPath.row]
        self.performSegue(withIdentifier: "ThirdToFourth", sender: self)
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "ThirdToFourth" {
            let vc = segue.destination as! FourthStepTVC
            vc.room = self.roomSelected
        }
    }
}

extension ThirdStepTVC: SectionHeaderDefaultDelegate {
    
    func didTapAddButton(in section: Int) {
        print("\(section)")
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Adicione o nome do cômodo", message: "", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "ex: Sala"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \((textField?.text)!)")
            User.sharedInstance.currentVisit?.rooms?.append(Room(title: (textField?.text)!))
            self.tableView.reloadData()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { [weak alert] (_) in
            alert?.dismiss(animated: true, completion: nil)
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
}
